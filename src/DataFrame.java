import java.util.ArrayList;
import java.util.Hashtable;

public class DataFrame {
	
	protected String[] names;
	protected String[] types;
	protected ArrayList<ArrayList> table = new ArrayList<ArrayList>();
	protected Hashtable<String, Integer> indices = new Hashtable<String, Integer>();
	
	
	public DataFrame(String[] _names, String[] _types)
	{
		names = _names;
		types = _types;
		
		if (names.length <= types.length && names.length != 0)
		{
			for(int i=0; i<names.length; i++)
			{
				indices.put(names[i], i);
				table.add(new ArrayList());
			}
		}
		else System.out.println("WARNING: Created an empty dataframe"); //I might change it to exception in the future
	}
	
	public DataFrame(DataFrame df)
	{
		names = df.names.clone();
		types = df.names.clone();
		indices = new Hashtable<String,Integer>(df.indices);
		for(ArrayList ar : df.table)
		{
			table.add(new ArrayList(ar));
		}
		
	}
	
	protected DataFrame(String[] _names, String[] _types, ArrayList[] cols, boolean copy)
	{
		//if (names.length <= types.length && names.length != 0)
		//{

			if(copy)
			{
				names = _names.clone();
				types = _types.clone();				
				for(int i=0; i<names.length; i++)
				{
					indices.put(names[i], i);
					table.add(new ArrayList(cols[i]));
				}
			}
			else
			{
				names = _names;
				types = _types;
				for(int i=0; i<names.length; i++)
				{
					if(names[i]=="int") names[i]="Integer";
					if(names[i]=="float") names[i]="Float";
					//dalej mi sie nie chce
					indices.put(names[i], i);
					table.add(cols[i]);
				}
			}
		//}
		//else System.out.println("WARNING: Created an empty dataframe"); //I might change it to exception in the future
	}
	
	protected DataFrame(String[] _names, String[] _types , ArrayList[] rows)
	{
		names = _names.clone();
		types = _types.clone();
		
		//if (names.length <= types.length && names.length != 0)
		//{	
			for(int i=0; i<names.length; i++)
			{
				indices.put(names[i], i);
				table.add(new ArrayList());
				for(int j=0; j<rows.length; j++)
				{
					table.get(i).add(rows[j].get(i));
				}
			}
		//}
		//else System.out.println("WARNING: Created an empty dataframe"); //I might change it to exception in the future
		//Those overloads are private and are mostly used for copying, so it isn't necessary to check if input is valid here.
	}
	
	
	public int size()
	{
		if (table.size()>0) return table.get(0).size();
		else return 0;
	}
	
	
	public ArrayList get(String colname)
	{
		if (indices.containsKey(colname)) return table.get(indices.get(colname));
		else return null;
	}
	
	
	public DataFrame get(String[] cols, boolean copy)
	{
		if(cols.length>0)
		{
			ArrayList [] arrayBuffer = new ArrayList[cols.length];
			String [] namesBuffer = new String[cols.length];
			String [] typesBuffer = new String[cols.length];
			for(int i=0; i<cols.length; i++)
			{
				if (indices.containsKey(cols[i]))
				{
					arrayBuffer[i] = table.get(indices.get(cols[i]));
					namesBuffer[i] = cols[i];
					typesBuffer[i] = types[indices.get(cols[i])];
				}
				else return null; //Null means that at least one of specified columns doesn't exist.
			}
			return new DataFrame(namesBuffer, typesBuffer, arrayBuffer, copy);
		}
		else return null; //Because creating it is pretty point(er)less. EEEEeeeeeEEEEEEEEeeeeeeEEEEEeeeeeeeeeee. Ok... Sorry... I'll go commit not breathing.
	}
	
	
	public DataFrame iloc(int index)
	{
		if (this.size()>0)
		{
			ArrayList elements[] = new ArrayList[1];
			for(int i=0; i<names.length; i++)
			{
				elements[0].add(table.get(i).get(index));
			}
			return new DataFrame(names, types, elements);
		}
		else return null;
	}
	
	
	public DataFrame iloc(int from, int to)
	{
		if (from >=0 && to < this.size() && to>from)
		{
			ArrayList[] elements = new ArrayList [to-from];
			for(int j=from; j<=to; j++)
			{
				for(int i=0; i<names.length; i++)
				{
					elements[j].add(table.get(i).get(j));
				}				
			}
			
			return new DataFrame(names, types, elements);	
		}
		else return null;
	}
}
