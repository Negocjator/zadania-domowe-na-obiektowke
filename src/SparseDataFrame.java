import java.util.ArrayList;
public class SparseDataFrame extends DataFrame {

	String hidden; 
	SparseDataFrame(String [] _names, String[] _types, String hide)
	{
		super(_names, _types);
		hidden = hide;
	}
	SparseDataFrame(DataFrame df, String hide)
	{
		super(df);
		hidden = hide;
		for(int i=0; i<table.size(); i++)
		{
			ArrayList<COOValue> bufferList = new ArrayList<COOValue>();
			for(int j=0; j<size(); j++)
			{
				if(!table.get(i).get(j).toString().equals(hidden)) bufferList.add(new COOValue(j, table.get(i).get(j)));
			}
			table.set(i, bufferList);
		}
		
	}
	
	
}
